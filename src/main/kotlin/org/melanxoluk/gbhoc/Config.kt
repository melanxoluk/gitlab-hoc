package org.melanxoluk.gbhoc

data class Config(
    val gitlabHost: String = "",
    val gitlabKey: String = "",
    val toPull: Boolean = true,
    val excludeProjects: List<Int> = listOf(),
    val author: String = "",
    val telegramKey: String = "",
    val telegramChat: Long = 0)
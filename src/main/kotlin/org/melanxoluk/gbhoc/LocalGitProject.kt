package org.melanxoluk.gbhoc

import org.gitlab4j.api.models.Project
import java.nio.file.Files
import java.nio.file.Path

object LocalGitProject {
    operator fun invoke(project: Project, toPull: Boolean): Path {
        val path = ProjectPath(project)

        if (Files.exists(path)) {
            if (toPull) GitPull(path)
        } else {
            GitClone(project.httpUrlToRepo, path)
        }

        return path
    }
}
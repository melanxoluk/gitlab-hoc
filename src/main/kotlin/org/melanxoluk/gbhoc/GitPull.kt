package org.melanxoluk.gbhoc

import java.nio.file.Path

object GitPull {
    operator fun invoke(path: Path): Int {
        println("start to pull in $path")

        return ProcessBuilder("git", "-C", """"$path"""", "pull")
            .inheritIO()
            .start()
            .waitFor()
    }
}
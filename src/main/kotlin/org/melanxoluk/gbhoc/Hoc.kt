package org.melanxoluk.gbhoc

import java.nio.file.Path
import kotlin.streams.toList

object Hoc {
    operator fun invoke(path: Path, author: String? = null): Long {
        val git =
            ProcessBuilder(
                listOf(
                    "git",
                    "-C",
                    "$path",
                    "log",
                    "--pretty=tformat:",
                    "--numstat",
                    "--ignore-space-change",
                    "--ignore-all-space",
                    "--ignore-submodules",
                    "--no-color",
                    "--find-copies-harder",
                    "-M",
                    "--diff-filter=ACDM",
                    author?.let { "--author=$it" } ?: "",
                    "--",
                    "."
                ).filter { it.isNotEmpty() }
            ).start()

        val hoc =
            git.inputStream
                .bufferedReader()
                .lines()
                .toList()
                .filter { it.isNotEmpty() }
                .fold(0L) { acc, s ->
                    val (_1, _2) = s.split("\t")
                    acc + l(_1) + l(_2)
                }

        return hoc
    }

    private fun l(any: Any): Long = try {
        any.toString().toLong()
    } catch (ex: Exception) {
        0L
    }
}
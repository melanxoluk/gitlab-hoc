package org.melanxoluk.gbhoc

import org.gitlab4j.api.models.Project
import java.nio.file.Path
import java.nio.file.Paths

object ProjectPath {
    operator fun invoke(project: Project): Path {
        return Paths.get("projects/${project.id} - ${project.name}")
    }
}
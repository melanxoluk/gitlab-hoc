package org.melanxoluk.gbhoc

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage
import org.gitlab4j.api.GitLabApi
import java.io.File
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*


object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val config =
            jacksonObjectMapper()
                .readValue<Config>(
                    File(args.firstOrNull() ?: "config.json")
                )

        val yesterday = Date.from(Instant.now().minus(1L, ChronoUnit.DAYS))

        val output =
            GitLabApi(config.gitlabHost, config.gitlabKey)
                .projectApi
                .memberProjects
                .filter { it.lastActivityAt.after(yesterday) }
                .mapNotNull { project ->
                    if (project.id in config.excludeProjects) {
                        System.err.println("skip project ${project.id} - ${project.nameWithNamespace}")
                        null
                    } else {
                        val hoc = Hoc(LocalGitProject(project, config.toPull), config.author)
                        // println("${project.nameWithNamespace} - $hoc")
                        project to hoc
                    }
                }.joinToString("\n") { (project, hoc) ->
                    String.format("%20s - %d", project.name, hoc)
                }

        val msg = buildString {
            appendln("```")
            appendln("HOC values:")
            appendln("---")
            appendln(output)
            appendln("```")
        }

        println()
        println()
        println(msg)

        TelegramBot(config.telegramKey).execute(
            SendMessage(config.telegramChat, msg)
                .parseMode(ParseMode.Markdown))
    }
}
package org.melanxoluk.gbhoc

import java.nio.file.Path

object GitClone {
    operator fun invoke(url: String, path: Path): Int {
        println("start to clone $url to $path")

        val cloneProcess =
            ProcessBuilder(
                "git",
                "clone",
                url,
                "$path")

        val exitCode =
            cloneProcess
                .inheritIO()
                .start()
                .waitFor()

        println("$url is downloaded with code $exitCode")

        return exitCode
    }
}
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.41"
}

group = "org.melanxoluk"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("ch.qos.logback:logback-classic:1.2.3")

    implementation("io.vertx:vertx-core:3.8.3")
    implementation("io.vertx:vertx-web:3.8.3")
    implementation("io.vertx:vertx-lang-kotlin:3.8.3")

    implementation("com.github.pengrad:java-telegram-bot-api:4.4.0")
    implementation("org.gitlab4j:gitlab4j-api:4.12.0")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.+")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}